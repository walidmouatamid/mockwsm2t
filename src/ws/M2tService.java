package ws;
import Facturiers.Facturier;
import Facturiers.FacturierReponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebService
public class M2tService {
    @WebMethod
    public FacturierReponse GetListeFacturiers(@WebParam(name = "SecurityMAC") String SecurityMAC,@WebParam(name = "AccepteurCod") String AccepteurCod,@WebParam(name = "CanalCod") String CanalCod,@WebParam(name = "FacturierCategorie") String FacturierCategorie,@WebParam(name = "DateOperationCanal") String DateOperationCanal,@WebParam(name = "AuditNumber") String AuditNumber,@WebParam(name = "CanalType") String CanalType,@WebParam(name = "lang") String lang){


        List<Facturier> ListeFacturiers=new ArrayList<>();
        ListeFacturiers.add(new Facturier("LBC Redal","0024",null,null,null,null));
        ListeFacturiers.add(new Facturier("Lydec","0001",null,null,null,null));
        ListeFacturiers.add(new Facturier("LBC Lydec","0022",null,null,null,null));
        ListeFacturiers.add(new Facturier("RADEEF","0026",null,null,null,null));
        ListeFacturiers.add(new Facturier("RAK","0027",null,null,null,null));
        ListeFacturiers.add(new Facturier("LBC Amendis","0028",null,null,null,null));
        ListeFacturiers.add(new Facturier("salafin","0045",null,null,null,null));
        ListeFacturiers.add(new Facturier("RADEEL","0032",null,null,null,null));
        ListeFacturiers.add(new Facturier("Redal","0002",null,null,null,null));
        ListeFacturiers.add(new Facturier("RADEEMA","0019",null,null,null,null));
        ListeFacturiers.add(new Facturier("ONEP","0013",null,null,null,null));
        ListeFacturiers.add(new Facturier("RADEEO","0025",null,null,null,null));
        ListeFacturiers.add(new Facturier("RADEES","0030",null,null,null,null));
        ListeFacturiers.add(new Facturier("Ramsa","0009",null,null,null,null));
        ListeFacturiers.add(new Facturier("Amendis","0011",null,null,null,null));
        ListeFacturiers.add(new Facturier("RADEETA","0047",null,null,null,null));
        ListeFacturiers.add(new Facturier("RADEEJ","0035",null,null,null,null));


        FacturierReponse facturierReponse= new FacturierReponse(String.valueOf((new Date())),"000","ACCEPTE",ListeFacturiers.size(),"45454",ListeFacturiers,"MDAwMjRUYwMDI3UkFLMDAyOExCQyBBbWVuZMFpu");
        return facturierReponse;
    }


}
