package Facturiers;


public class Params {
    String DataName;
    String DataVal;

    public Params() {
    }

    public Params(String dataName, String dataVal) {
        DataName = dataName;
        DataVal = dataVal;
    }

    public String getDataName() {
        return DataName;
    }

    public void setDataName(String dataName) {
        DataName = dataName;
    }

    public String getDataVal() {
        return DataVal;
    }

    public void setDataVal(String dataVal) {
        DataVal = dataVal;
    }
}
