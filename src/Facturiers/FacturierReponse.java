package Facturiers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class FacturierReponse {
    String DateOperationServeur;
    String ErrorCod;
    String ErrorMsg;
    int NbFacturiers;
    String AuditNumber;
    List<Facturier> ListeFacturiers;
    String SecurityMAC;
    public FacturierReponse() {
    }

    public FacturierReponse(String dateOperationServeur, String errorCod, String errorMsg, int nbFacturiers, String auditNumber, List<Facturier> listeFacturiers, String securityMAC) {
        DateOperationServeur = dateOperationServeur;
        ErrorCod = errorCod;
        ErrorMsg = errorMsg;
        NbFacturiers = nbFacturiers;
        AuditNumber = auditNumber;
        ListeFacturiers = listeFacturiers;
        SecurityMAC = securityMAC;
    }

    public String getDateOperationServeur() {
        return DateOperationServeur;
    }

    public void setDateOperationServeur(String dateOperationServeur) {
        DateOperationServeur = dateOperationServeur;
    }

    public String getErrorCod() {
        return ErrorCod;
    }

    public void setErrorCod(String errorCod) {
        ErrorCod = errorCod;
    }

    public String getErrorMsg() {
        return ErrorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        ErrorMsg = errorMsg;
    }

    public int getNbFacturiers() {
        return NbFacturiers;
    }

    public void setNbFacturiers(int nbFacturiers) {
        NbFacturiers = nbFacturiers;
    }

    public String getAuditNumber() {
        return AuditNumber;
    }

    public void setAuditNumber(String auditNumber) {
        AuditNumber = auditNumber;
    }
    @XmlElementWrapper(name="ListeFacturiers")
    @XmlElement(name="item")
    public List<Facturier> getListeFacturiers() {
        return ListeFacturiers;
    }

    public void setListeFacturiers(List<Facturier> listeFacturiers) {
        ListeFacturiers = listeFacturiers;
    }

    public String getSecurityMAC() {
        return SecurityMAC;
    }

    public void setSecurityMAC(String securityMAC) {
        SecurityMAC = securityMAC;
    }
}
