package Facturiers;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class Facturier {
    String FacturierName;
    String FacturierCod;
    @XmlElement(nillable=true)
    String FacturierDesc;
    @XmlElement(nillable=true)
    String FacturierLogo;
    @XmlElement(nillable=true)
    String FacturierWebSite;
    @XmlElement(nillable=true)
    List<Params> Params;

    public Facturier() {
    }

    public Facturier(String facturierName, String facturierCod, String facturierDesc, String facturierLogo, String facturierWebSite, List<Facturiers.Params> params) {
        FacturierName = facturierName;
        FacturierCod = facturierCod;
        FacturierDesc = facturierDesc;
        FacturierLogo = facturierLogo;
        FacturierWebSite = facturierWebSite;
        Params = params;
    }

    public String getFacturierName() {
        return FacturierName;
    }

    public void setFacturierName(String facturierName) {
        FacturierName = facturierName;
    }

    public String getFacturierCod() {
        return FacturierCod;
    }

    public void setFacturierCod(String facturierCod) {
        FacturierCod = facturierCod;
    }

    public String getFacturierDesc() {
        return FacturierDesc;
    }

    public void setFacturierDesc(String facturierDesc) {
        FacturierDesc = facturierDesc;
    }

    public String getFacturierLogo() {
        return FacturierLogo;
    }

    public void setFacturierLogo(String facturierLogo) {
        FacturierLogo = facturierLogo;
    }

    public String getFacturierWebSite() {
        return FacturierWebSite;
    }

    public void setFacturierWebSite(String facturierWebSite) {
        FacturierWebSite = facturierWebSite;
    }

    public List<Facturiers.Params> getParams() {
        return Params;
    }

    public void setParams(List<Facturiers.Params> params) {
        Params = params;
    }
}
