package server;

import ws.M2tService;

import javax.xml.ws.Endpoint;

public class JaxWsServer {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8085/",new M2tService());
        System.out.println("WSDL url is: http://localhost:8085/m2tservice?wsdl");
    }
}
